#ifndef MATRIX_TOOLS
#define MATRIX_TOOLS

#include <stdio.h>

typedef struct{
	int *array;
	int rows, cols, determinant;
}matrix_t;

/*
*Implemented functions for matrix
*
*/

matrix_t* matrix_init(int rows, int cols); //create and initaliaze matrix from 1 to N+1

matrix_t* read_matrix_ff(FILE* file); //create matrix based on input form file [rows] [cols] [input]

matrix_t* copy_matrix(matrix_t* matrix); //create new instance of matrix

void transpose_matrix(matrix_t* m); //transpose matrix

void print_matrix(matrix_t* m); //print single matrix

void free_matrix(matrix_t* m); //free matrix from memory

matrix_t* matrix_multiplication(matrix_t* a, matrix_t* b);

int cmp_size_matrix(matrix_t* a, matrix_t* b); //compare two matrix designed on cols and rows, return 1(on true)

int is_squere(matrix_t* m); //return 1 if matrix is squere

int compare_matrix(matrix_t* a, matrix_t* b); // return 1 if matrix contain same members

int is_regular(matrix_t* m); //1 true, -1 false

int get_determinant(matrix_t* m); //returns a determinant of matrix (program remembers once it is calculated)

int matrix_rank(matrix_t* m); //not implemented yet

void multiply_by_scalar(matrix_t* m, int num); //all members of matrix will be multiplied by number

matrix_t* get_row(matrix_t* m, int num); //return row as a new matrix

int calculate_gcd(int a, int b); //returns greatest common division of two numbers


#endif