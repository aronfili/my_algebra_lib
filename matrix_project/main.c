#include <stdio.h>
#include <stdlib.h>
#include "matrix_tools.h"

int main(int args, char**argv){
	
	matrix_t* A = matrix_init(2,3);
	matrix_t* B = matrix_init(3,2);
	matrix_t* C = matrix_multiplication(A,B);
	print_matrix(A);
	print_matrix(B);
	print_matrix(C);
	
	is_regular(A);
	is_regular(B);
	is_regular(C);
	
	printf("determinant is = %d\n", get_determinant(C));
	
	matrix_t* R_0 = get_row(C,0);
	printf("this is first row of third matrix\n");
	print_matrix(R_0);
	
	printf("You finish some code and you realized you haven't done a good analyze,\nbecause  your matrix doesn't have a parametr name, and you can't print the one you mean.\n");
	free_matrix(A);
	free_matrix(B);
	free_matrix(C);
	free_matrix(R_0);
	return 0;	
}