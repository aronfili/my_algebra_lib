#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define MAX_W_SPACE 4 //set max white spaces  between numbers in "print_number" function

void print_number(int number); //makes space to print matrix formated
void print_matrix(int *matrix, int rows, int cols);
void transpose_matrix(int *matrix, int *rows, int *cols);

int main(int args, char** argv){
	//HEAD
	int *matrix;
	int rows;
	int cols;
	
	printf("Hello,\nmy name is Filip Aron and I want to show you what I can do in C and\nshow you my deep knowledge of programing and enthusiasm for it.\n\n");
	
	printf("I use stdio.h to read input from you.\n\n");
	printf("Let's make a simple matrix.\nTell me number of rows:");
	if(1 != scanf("%d", &rows)){
		fprintf(stderr, "wrong input.\n");
		return 1;
	}
	printf("Tell me number of cols:");
	if(1 != scanf("%d", &cols)){
		fprintf(stderr, "wrong input.\n");
		return 1;
	}
	printf("\n");
	printf("If you want to create dynamicly alocated object you should use stdlib.h (there are another ways to do it)\n");
	
	matrix = (int*) malloc(sizeof(int*)*rows*cols);
	for(int i = 0 ; i < rows*cols; i ++){
		matrix[i] = i;
	}
	
	print_matrix(matrix, rows, cols);
	sleep(5);
	printf("\nMost important thing is to choose a right algorithm to solve your problem.\nImagine you want to define operations with matrix.\n");
	sleep(5);
	printf("\ntransposition\n\n");
	transpose_matrix(matrix, &rows, &cols);
	print_matrix(matrix, rows, cols);
	printf("\nsum of same size\n?\n");
	printf("\nmultiplication\n?\n\n");
	printf("then we will find out that we need to create objects. In C are structures.\n");
	printf("Previous abstraction wasn't the best we used 3 variables to store integers in matrix.\nIt's fast but sometimes we need more abstraction, because it can start to be messy. We usually need more abstraction.\n");
	printf("so best option is create C like libraries for work with certain things.\n");
	
	free(matrix);
	
	//BIG_LINE********************************************
	
	
	
	
	return 0;
}

void transpose_matrix(int *matrix, int *rows, int *cols){
	int r = *cols;
	int c = *rows;
	int *m = malloc(sizeof(int)*r*c);
	int c_i = 0;
	int r_i = 0;
	memcpy(m,matrix, sizeof(int)*r*c);
	for(int i = 0 ; i < r; i++){
		for(int j = 0; j < c; j++){
			matrix[i*c+j] = m[r_i*r+c_i];
			r_i++;
			if(r_i == c){
				r_i = 0;
				c_i++;
			}
		}
	}
	*cols = c;
	*rows = r;
	free(m);
}

void print_number(int number){
	int num = number;
	int space = 0;
	while(num > 9){
		num = num / 10;
		space++;
	}
	putchar(' ');
	printf("%d",number);
	for(int i = 0; i < (MAX_W_SPACE - space); i++){
		putchar(' ');
	}
}

void print_matrix(int *matrix, int rows, int cols){
	for(int i = 0; i < rows; i ++){
		for(int j = 0; j < cols; j++){
			print_number(matrix[i*cols+j]);
		}
		putchar('\n');
	}
}