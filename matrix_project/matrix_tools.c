#include "matrix_tools.h"
#include <string.h>
#include <stdlib.h>

#define DET_FAIL -9999
#define MAX_W_SPACE 4

void print_number(int number);
matrix_t* submatrix(matrix_t* a, int row, int col);
int count_determinant(matrix_t* m);

matrix_t* matrix_init(int rows, int cols){
	matrix_t* m = (matrix_t*) malloc(sizeof(matrix_t*));
	m->array = (int*) malloc(sizeof(int) * rows * cols);
	m->rows = rows;
	m->cols = cols;
	m->determinant = DET_FAIL;
	for(int i = 0; i < rows*cols; i++){
		m->array[i]= i+1;
	}
	return m;
}

matrix_t* copy_matrix(matrix_t* matrix){
	matrix_t* m = (matrix_t*) malloc(sizeof(matrix_t*));
	m->array = (int*) malloc(sizeof(int) * matrix->rows * matrix->cols);
	m->rows = matrix->rows;
	m->cols = matrix->cols;
	m->determinant = DET_FAIL;
	memcpy(m->array, matrix->array, sizeof(int)* matrix->rows*matrix->cols);
	return m;
}

matrix_t* read_matrix_ff(FILE* file){
	int r, c; 
	if(fscanf(file, "%d %d", &r, &c) != 2){
		fprintf(stderr, "reading matrix wasn't succesfull\n");
		return NULL;
	}
	matrix_t* m = (matrix_t*) malloc(sizeof(matrix_t*));
	m->array = (int*) malloc(sizeof(int)*r*c); 
	m->cols = c;
	m->rows = r;
	m->determinant = DET_FAIL;
	for(int i = 0 ; i < m->rows; i++){
		for(int j = 0;  j <  m->cols; j++){
			scanf("%d", &m->array[i*m->cols+j]);
		}
	}
	return m;
}

void transpose_matrix(matrix_t* m){
	int r = m->cols;
	int c = m->rows;
	int c_i = 0;
	int r_i = 0;
	matrix_t *copy = copy_matrix(m);
	for(int i = 0 ; i < r; i++){
		for(int j = 0; j < c; j++){
			m->array[i*c+j] = copy->array[r_i*r+c_i];
			r_i++;
			if(r_i == c){
				r_i = 0;
				c_i++;
			}
		}
	}
	m->rows = r;
	m->cols = c;
	free_matrix(copy);
}

void print_matrix(matrix_t* m){
	for(int i = 0; i < m->rows; i++){
		for(int j = 0; j< m->cols; j++){
			print_number(m->array[i*m->cols+j]);
		}
		putchar('\n');
	}
	putchar('\n');
}

void free_matrix(matrix_t* m){
	if(m != NULL){
		free(m->array);
		free(m);
	}
}

int compare_matrix(matrix_t* a, matrix_t* b){
	if(cmp_size_matrix(a,b)){
		for(int i = 0 ; i < a->rows; i++){
			for(int j = 0 ; j < a->cols; j++){
				if(a->array[i*a->cols+j] != b->array[i*b->cols+j]){
					fprintf(stderr,"matrix hasn't same members\n");
					return 0;
				}
			}
		}
	}
	fprintf(stderr,"matrix has same members\n");
	return 1;
}

int cmp_size_matrix(matrix_t* a, matrix_t* b){
	if(a->rows == b->rows && a->cols == b->cols) return 1;
	return 0;
}

int is_squere(matrix_t* m){
	if(m->rows == m->cols)return 1;
	return 0;
}

int is_regular(matrix_t* m){//1 true, -1 false
	int det = get_determinant(m);
	if(det == 0 || det == DET_FAIL){
		printf("Matrix isn't regular.\n");
		return 0;
	}
	printf("Matrix is regular.\n");
	return 1;
}

int get_determinant(matrix_t* m){
	if(m->determinant != DET_FAIL){
		return m->determinant;
	}
	return count_determinant(m);
}

matrix_t* matrix_multiplication(matrix_t* a, matrix_t* b){
	if(!(a->cols == b->rows)){
		fprintf(stderr,"Matrix are not possible to multiply.\n");
		return NULL;
	}
	matrix_t* tmp = (matrix_t*) malloc(sizeof(matrix_t*));
	tmp->array = (int*) malloc(sizeof(int) * (a->rows) * (a->cols));
	tmp->rows = a->rows;
	tmp->cols = b->cols;
	tmp->determinant = DET_FAIL;
	for(int i = 0; i < tmp->rows; i++){
		for(int j = 0; j < tmp->cols; j++){
			int sum = 0;
			for(int n = 0 ; n < a->cols; n++){
				sum+= a->array[i*a->cols+n]* b->array[n*b->cols+j];
			}
			tmp->array[i*tmp->cols+j] = sum;
		}
	}
	return tmp;
	
}

int matrix_rank(matrix_t* m){
	
	
	return 0;
}

void multiply_by_scalar(matrix_t* m, int num){
	for(int i = 0 ; i < m->rows; i++){
		for(int j = 0; j < m->cols; j++){
			m->array[i*m->cols+j] = m->array[i*m->cols+j]*num;
		}
	}
}

matrix_t* get_row(matrix_t* m, int num){
	matrix_t* row = matrix_init(1,m->cols);
	memcpy(row->array,m->array+(num*m->cols),m->cols*sizeof(int));
	return row;
}

int calculate_gcd(int a, int b){
	a = (a>0)?a:-a;
	b = (b>0)?b:-b;
	while(a != b){
		if(a > b){
			a -= b;
		}else{
			b -= a;
		}
	}
	return a;
}

//Private Functions

int count_determinant(matrix_t* m){
	if(!is_squere(m)){
		fprintf(stderr,"ERROR Determinant is not posible to calculate.\n");
		return DET_FAIL;
	}
	
	int det = 0;
    //  Base case : if matrix contains single element 
    if (m->rows == 1) 
        return m->array[0]; 
  
    matrix_t* tmp;
    int sign = 1;  
     // Iterate for each element of first row 
    for (int f = 0; f < m->rows; f++){
        tmp = submatrix(m,0, f);
        det += sign * m->array[f] * count_determinant(tmp);
		  free(tmp);
        sign = -sign; 
    } 
  
    return det; 
}

matrix_t* submatrix(matrix_t* a, int row, int col){
	matrix_t* sub = (matrix_t*) malloc(sizeof(matrix_t*));
	sub->array = (int*) malloc(sizeof(int) * (a->rows-1) * (a->cols-1));
	sub->rows = a->rows -1;
	sub->cols = a->cols -1;
	sub->determinant = DET_FAIL;
	int ra = 0;
	int ca;
	
	for(int i = 0 ; i < sub->rows; i++){
		ca = 0;
		if(i == row){
			ra = 1;
		}
		for(int j = 0; j < sub->cols; j++){
			if(j == col){
				ca++;
			}
			sub->array[i*sub->cols+j] = a->array[(i+ra)*a->cols+(j+ca)];
		}
	}
	return sub;
}
void print_number(int number){
	int num = number;
	int space = 0;
	while(num > 9){
		num = num / 10;
		space++;
	}
	putchar(' ');
	printf("%d",number);
	for(int i = 0; i < (MAX_W_SPACE - space); i++){
		putchar(' ');
	}
}
